<?php

include_once "context.php";
include_once "states.php";
include_once "concrete_states.php";
include_once "messages.php";
include_once "sql_queries.php";
include_once "db.php";

// Start simulation script...
// Insert card in ATM and write pin code
$pinCode = 2022;
$context = new AtmContext(new CardInsertedInAtm()); // Card was inserted
$context->executeStateAction1($pinCode); // Enter pin code and check it

// Select transaction in main menu...
$selectedMenuNumber = 1;

switch ($selectedMenuNumber) {
    case 1:
        // In menu was selected first item from the list (Withdraw)
        $moneySummValue = 200;
        $context->executeStateAction1($moneySummValue);
        break;
    case 2:
        // In menu was selected second item from the list (Display balance)
        $context->executeStateAction2($value);
        break;
    case 3:
        // In menu was selected third item from the list (Exit)
        $context->executeStateAction3();
        break;
    default:
        echo "******* Select correct transaction! ********";
}
