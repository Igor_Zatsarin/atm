<?php

include_once "index.php";

class SqlQueries
{
    private $account;

    public function __constract()
    {
        $db = new DatabaseConnection();
        $this->conn = $db->conn;
    }

    public function authenticatePin($pinValue)
    {
        /* For the demo version without a frontend, database search is not used

        $sql = "SELECT * FROM cards WHERE pin = '$pinValue'";
        $result = $this->conn->query($sql);
        if($result){
            while ($row = $result->fetch_assoc()) {
                $this->$accountNumber = $row[accountNumber]; // Находимо номер рахунку клієнта
            }
            return true;
        }else{
            return false;
        }
        */

        // For the demo version without a frontend, we enter the data automatically
        $this->account = 1234567890;
        if ($pinValue == 2022) {
            return true;
        } else {
            return false;
        }
    }

    public function getAccountNumber()
    {
        return $this->account;
    }

    public function checkIfCardBlocked()
    {
        /* For the demo version without a frontend, database search is not used

        $sql = "SELECT * FROM cards WHERE accountNumber = '$this->account'";
        $result = $this->conn->query($sql);
        if($result){
            while ($row = $result->fetch_assoc()) {
                $cardIsBlocked = $row[blocked];
            }
            if($cardIsBlocked) {
                return true;
            }else{
                return false;
            }
        }
        */

        // For the demo version without a frontend, we enter the data automatically
        $cardIsBlocked = false;
        if ($cardIsBlocked) {
            return true;
        } else {
            return false;
        }
    }

    public function checkBalance()
    {
        /* For the demo version without a frontend, database search is not used

        $sql = "SELECT * FROM cards WHERE accountNumber = '$this->account'";
        $result = $this->conn->query($sql);
        if($result){
            while ($row = $result->fetch_assoc()) {
                $balance = $row[moneyBalance];
            }
            if($balance > 0) {
                return $balance;
            }else{
                $zero = "0";
                return $zero;
            }
        }

        */

        // For the demo version without a frontend, we enter the data automatically
        $balance = 255;
        return $balance;
    }

    public function saveNewBalance($newBalance): void
    {
        /* For the demo version without a frontend, database search is not used

        $sql = "INSERT INTO cards (moneyBalance) VALUES('$newBalance')";
        $sql = "SELECT * FROM cards WHERE accountNumber = '$this->account'";
        $result = $this->conn->query($sql);
        if ($result === false) {
            die($db->error);
        }
        mysqli_close($db);

        */
    }
}
