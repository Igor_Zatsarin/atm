<?php

include_once "index.php";

abstract class AtmStates
{
    protected $context;

    public function setAtmContext(AtmContext $context): void
    {
        $this->context = $context;
    }

    public function showStateMsg($msgVariant): void
    {
        $msg = new AtmMessages();
        $msg->$msgVariant();
    }

    abstract public function stateAction1($value);
    abstract public function stateAction2($value);
    abstract public function stateAction3();
}
