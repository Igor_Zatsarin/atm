<?php

include_once "./index.php";

class AtmContext
{
    private $state;

    public function __construct(AtmStates $state)
    {
        $this->setState($state);
    }

    public function setState(AtmStates $state): void
    {
        $this->state = $state;
        $this->state->setAtmContext($this);
    }

    public function executeStateAction1($value): void
    {
        $this->state->stateAction1($value);
    }

    public function executeStateAction2($value): void
    {
        $this->state->stateAction2($value);
    }

    public function executeStateAction3(): void
    {
        $this->state->stateAction3();
    }
}
