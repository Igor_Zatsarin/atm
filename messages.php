<?php

include_once "index.php";

class AtmMessages
{
    public function echoMsg($msg)
    {
        echo $msg."<br>";
    }

    public function cardWasEntered()
    {
        $this->echoMsg("****** Debit card was inserted ******");
        $this->echoMsg("");
    }

    public function enterPinCode()
    {
        $this->echoMsg("****** Enter PIN code please ******");
        $this->echoMsg("");
        $this->echoMsg("Simulation PIN code was entered automatically");
        $this->echoMsg("");
    }

    public function pinCodeError()
    {
        $this->echoMsg("");
        $this->echoMsg("****** Wrong PIN! ******");
    }

    public function mainMenuList()
    {
        $this->echoMsg("");
        $this->echoMsg("");
        $this->echoMsg("************* MAIN MENU *************");
        $this->echoMsg("");
        $this->echoMsg("1) Withdraw cash");
        $this->echoMsg("");
        $this->echoMsg("2) Display balance");
        $this->echoMsg("");
        $this->echoMsg("3) Exit");
        $this->echoMsg("");
        $this->echoMsg("*************************************");
    }

    public function mainMenuListWithdraw()
    {
        $this->echoMsg("");
        $this->echoMsg("****** Withdraw cash ******");
    }

    public function cardIsBlockedError()
    {
        $this->echoMsg("");
        $this->echoMsg("****** Your banks account is blocked ******");
    }

    public function moneyNotEnoughError()
    {
        $this->echoMsg("");
        $this->echoMsg("****** Insufficient funds in the account! Input another amount! ******");
    }

    public function succesWithdrawalOperation()
    {
        $this->echoMsg("");
        $this->echoMsg("****** Money withdrawal operation is successful! ******");
    }

    public function cardPushOutMsg()
    {
        $this->echoMsg("");
        $this->echoMsg("**************** EXIT ****************");
        $this->echoMsg("");
        $this->echoMsg("****** Dont forget your card in ATM! ******");
    }
}
