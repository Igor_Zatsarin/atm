<?php

include_once "index.php";

class DatabaseConnection
{
    private $host = "localhost";
    private $db_name = "";
    private $username = "";
    private $password = "";
    public $conn;

    // connection with data base
    public function getConnection()
    {
        $this->conn = null;
        $this->conn = new mysqli($this->host, $this->username, $this->password, $this->db_name);
        if (!$this->conn) {
            die("Connection failed: " . mysqli_connect_error());
        }

        return $this->conn;
    }
}
