<?php

include_once "index.php";

class CardInsertedInAtm extends AtmStates
{
    public function __construct()
    {
        $this->showStateMsg("cardWasEntered");
        $this->showStateMsg("enterPinCode");
    }

    public function stateAction1($pinCode): void 
    {
        $this->context->setState(new EnteredPinCodeCheck());
        $this->context->executeStateAction1($pinCode);
    }

    public function stateAction2($value): void {}

    public function stateAction3(): void {}
}

class EnteredPinCodeCheck extends AtmStates
{
    public function stateAction1($pinCode): void
    {
        $sqlObject = new SqlQueries();
        $pinCheckValue = $sqlObject->authenticatePin($pinCode);
        // Save class in session
        $regSerlizer = base64_encode(serialize($sqlObject));
        $_SESSION['regSession'] = $regSerlizer;

        if ($pinCheckValue) {
            $this->context->setState(new ShowMainMenu());
            $this->context->setState(new SelectTransaction());
        } else {
            $this->showStateMsg("pinCodeError");
            $this->context->setState(new EndAllAndExit());
        }
    }

    public function stateAction2($value): void {}

    public function stateAction3(): void {}
}

class ShowMainMenu extends AtmStates
{
    public function __construct()
    {
        $this->showStateMsg("mainMenuList");
    }

    public function stateAction1($value): void {}

    public function stateAction2($value): void {}

    public function stateAction3(): void {}
}

class SelectTransaction extends AtmStates
{
    public function stateAction1($moneyValue): void
    {
        $this->context->setState(new DisplayBalance());
        $this->context->executeStateAction1($value);
        $this->context->setState(new EnterWithdrawalAmount());
        $this->context->executeStateAction1($moneyValue);
    }

    public function stateAction2($value): void
    {
        $this->context->setState(new DisplayBalance());
        $this->context->executeStateAction1($value);
    }

    public function stateAction3(): void
    {
        $this->context->setState(new EndAllAndExit());
    }
}

class EnterWithdrawalAmount extends AtmStates
{
    private $newBalance;

    public function stateAction1($moneyValue): void
    {
        $regUnserilizeObj = unserialize((base64_decode($_SESSION['regSession'])));
        $cardIsBlocked = $regUnserilizeObj->checkIfCardBlocked();

        if ($cardIsBlocked) {
            $this->showStateMsg("cardIsBlockedError");
            $this->context->setState(new EndAllAndExit());
            $this->context->state = null;
        } else {
            // Restore from session saved class
            $balance = $regUnserilizeObj->checkBalance();
            $this->newBalance = $balance - $moneyValue;

            if ($this->newBalance >= 0) {
                $regUnserilizeObj->saveNewBalance($this->newBalance);
                $this->showStateMsg("succesWithdrawalOperation");
                $this->context->setState(new DisplayBalance());
                $this->context->executeStateAction2($this->newBalance);
                $this->context->setState(new SelectTransaction());
            } else {
                $this->showStateMsg("moneyNotEnoughError");
                $this->context->setState(new SelectTransaction());
            }
        }
    }

    public function stateAction2($value): void {}

    public function stateAction3(): void {}
}

class DisplayBalance extends AtmStates
{
    public function stateAction1($value): void
    {
        // Restore from session saved class
        $regUnserilizeObj = unserialize((base64_decode($_SESSION['regSession'])));
        $balance = $regUnserilizeObj->checkBalance();
        $this->stateAction2($balance);
    }

    public function stateAction2($balance): void
    {
        echo "<br>";
        echo "Money balance: $balance"."<br>";
    }

    public function stateAction3(): void {}
}

class EndAllAndExit extends AtmStates
{
    public function __construct()
    {
        $this->showStateMsg("cardPushOutMsg");
    }

    public function stateAction1($value): void {}

    public function stateAction2($value): void {}

    public function stateAction3(): void {}
}
